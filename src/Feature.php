<?php

namespace Drupal\geolocation_provider;

/**
 * Provides Feature class.
 *
 * @package Drupal\geolocation_provider
 */
class Feature {

  /**
   * The score of the feature.
   *
   * @var int
   */
  public $score;

  /**
   * The y-coordinate of the feature.
   *
   * @var float
   */
  public $y;

  /**
   * The x-coordinate of the feature.
   *
   * @var float
   */
  public $x;

  /**
   * The importance of the feature.
   *
   * @var int
   */
  public $importance;

  /**
   * The house number of the feature.
   *
   * @var string
   */
  public $housenumber;

  /**
   * The postcode of the feature.
   *
   * @var string
   */
  public $postcode;

  /**
   * The context of the feature.
   *
   * @var string
   */
  public $context;

  /**
   * The ID of the feature.
   *
   * @var string
   */
  public $id;

  /**
   * The street name of the feature.
   *
   * @var string
   */
  public $street;

  /**
   * The city name of the feature.
   *
   * @var string
   */
  public $city;

  /**
   * The label of the feature.
   *
   * @var string
   */
  public $label;

  /**
   * The type of the feature.
   *
   * @var string
   */
  public $type;

  /**
   * The city code of the feature.
   *
   * @var string
   */
  public $citycode;

  /**
   * The name of the feature.
   *
   * @var string
   */
  public $name;

  /**
   * The geometry of the feature.
   *
   * @var array|null
   */
  public $geometry;

  /**
   * Feature constructor.
   *
   * @param array $data
   *   Json decoded data.
   */
  public function __construct($data) {
    $properties = $data['properties'] ?? [];
    $this->geometry = $data['geometry'] ?? NULL;
    $this->score = $properties['score'] ?? -1;
    $this->y = $properties['y'] ?? 0;
    $this->x = $properties['x'] ?? 0;
    $this->importance = $properties['importance'] ?? 0;
    $this->housenumber = $properties['housenumber'] ?? '';
    $this->postcode = $properties['postcode'] ?? '';
    $this->context = $properties['context'] ?? '';
    $this->id = $properties['id'] ?? '';
    $this->street = $properties['street'] ?? '';
    $this->city = $properties['city'] ?? '';
    $this->label = $properties['label'] ?? '';
    $this->type = $properties['type'] ?? '';
    $this->citycode = $properties['citycode'] ?? '';
    $this->name = $properties['name'] ?? '';
  }

}
