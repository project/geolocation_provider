<?php

namespace Drupal\geolocation_provider\Encoder;

use Drupal\geolocation_provider\FeatureCollection;
use Symfony\Component\Serializer\Encoder\JsonEncoder;

/**
 * Override JsonEncoder for GeoJSON.
 *
 * @package Drupal\geolocation_provider\Encoder
 */
class GeoJsonEncoder extends JsonEncoder {

  /**
   * The format that this encoder supports.
   *
   * @var string[]
   */
  protected static array $formats = ['geojson'];

  /**
   * {@inheritdoc}
   */
  public function supportsDecoding($format, array $context = []): bool {
    return in_array($format, static::$formats, TRUE);
  }

  /**
   * {@inheritdoc}
   */
  public function decode($data, string $format, array $context = []): ?FeatureCollection {
    $json = parent::decode($data, $format, $context);
    if (isset($json['type']) && $json['type'] === 'FeatureCollection') {
      return new FeatureCollection($json['features']);
    }
    return NULL;
  }

}
